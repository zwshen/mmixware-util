//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MMIX-Edit.rc
//
#define IDC_MYICON                      2
#define IDD_MMIXEDIT_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MMIXEDIT                    107
#define IDI_SMALL                       108
#define IDC_MMIXEDIT                    109
#define IDR_MAINFRAME                   128
#define IDD_MAIN                        129
#define IDD_MMIXABOUT                   131
#define IDD_ERROR                       132
#define IDC_CONSOLEOUTPUT               1017
#define IDC_ERRORLIST                   1021
#define IDC_CUSTOM2                     1023
#define IDC_TAB1                        1024
#define IDC_MFCPROPERTYGRID1            1030
#define IDC_CUSTOM1                     1031
#define ID_DATEI_SPEICHERN              32771
#define ID_DATEI_SPEICHERNUNTER         32772
#define ID_DATEI_32773                  32773
#define ID_DATEI_NEU                    32774
#define ID_HILFE_32775                  32775
#define ID_BEARBEITEN_UNDO              32776
#define ID_BEARBEITEN_WIEDERHOLEN       32777
#define ID_BEARBEITEN_AUSSCHNEIDEN      32778
#define ID_BEARBEITEN_KOPIEREN          32779
#define ID_BEARBEITEN_EINF32780         32780
#define ID_BEARBEITEN_L32781            32781
#define ID_BEARBEITEN_ALLESAUSW32782    32782
#define ID_MMIX_AUSF32785               32785
#define IDM_Run                         32786
#define ID_FILE_NEW                     32790
#define ID_FILE_OPEN                    32791
#define ID_FILE_SAVE                    32792
#define ID_FILE_SAVEAS                  32793
#define ID_FILE_EXIT                    32794
#define ID_EDIT_UNDO                    32797
#define ID_EDIT_REDO                    32798
#define ID_EDIT_CUT                     32799
#define ID_EDIT_COPY                    32800
#define ID_EDIT_PASTE                   32801
#define ID_EDIT_DELETE                  32802
#define ID_EDIT_SELECTALL               32803
#define ID_HELP_ABOUT                   32804
#define ID_MMIX                         32805
#define ID_MMIX_RUN                     32806
#define WM_RUN                          32807
#define IDD_MENU_RUN                    32808
#define ID_SETTINGS                     32809
#define ID_MMIX_COMPILE                 32810
#define ID_MMIX_ASSEMBLIEREN            32811
#define IDD_MENU_COMPILE                32812
#define ID_ACCELERATOR32828             32828
#define IDM_FILE_NEW                    40001
#define IDM_FILE_OPEN                   40002
#define IDM_FILE_SAVE                   40003
#define IDM_FILE_SAVEAS                 40004
#define IDM_FILE_EXIT                   40005
#define IDM_EDIT_CUT                    40006
#define IDM_EDIT_COPY                   40007
#define IDM_EDIT_UNDO                   40008
#define IDM_EDIT_REDO                   40009
#define IDM_EDIT_PASTE                  40010
#define IDM_EDIT_DELETE                 40011
#define IDM_EDIT_SELECTALL              40012
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32829
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
